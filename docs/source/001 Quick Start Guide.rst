Quick Start Guide
=================

Installation Guide
------------------

1. Embed the JavaScript snippet
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Drop this code into the ``head`` of your site, this will asynchronously
load our JavaScript library onto your page so you can use it to send us
data. **Be sure to substitute in your own token:**

.. code:: javascript

    <script type="text/javascript">
      (function() {
        window.tio = window.tio || [];
        var d = document, f = d.getElementsByTagName('script')[0],
        t = 'YOUR_TOKEN_HERE',
        s = d.createElement('script'), m = ['track', 'identify','setProperties'],
        r = function (e) {return function () { tio.push([e].concat(Array.prototype.slice.call(arguments, 0))) }};
        s.type = 'text/javascript'; s.async = true; s.src = 'https://tracktor-dev.s3.amazonaws.com/scripts/'+t+'.js';
        f.parentNode.insertBefore(s, f); for(var i in m) tio[m[i]] = r(m[i]);YS=tio; })();
    </script>

2. Identify a Person & Send Data
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you have data about a person you want to send, such as their name,
age, email address, etc, you can use the ``tio.identify`` and
``tio.setProperties`` methods:

.. code:: javascript

    tio.identify('unicorn'); // <-- login of your user, or any other unique string that identifies user and doesn't change.
    tio.setProperties({
      email: 'unicorn@example.com',
      first_name: 'Dave',
      last_name: 'Johnson',
      age: 25,
      billing_plan: 'free'}); // + any other info about your user.

3. Track an Event
~~~~~~~~~~~~~~~~~

Events are actions your users do in your app. Once you have the snippet
in your page, you can track an event by calling ``tio.track`` with the
event name, and also properties as key value pairs:

.. code:: javascript

    tio.track('rated-product', {
      productid: 33156,
      category: 'Consumer electronics',
      rating: 5,
      comment: 'Very good product!'});
