Overview
========

**Tracktor.io** is an analytics product that shows you who uses your
application and helps you to easily communicate with them via letters
and in-app messages. With Tracktor.io you will receive at least the
following information:

-  The most popular and attractive pages of your site.
-  At which steps your users usually stop while purchasing, upgrading or
   installing. Who these users are.
-  Which advertisement campaigns bring active customers to your site.
-  Which customers are leaving your site. You can ask them why they
   leave.

Sending Data to Tracktor.io
---------------------------

Tracktor.io features simple and easy to use HTTP api. Also, we have
pre-built libraries for various languages. If you just want to start as
quickly as possible, follow `Quick Start Guide <#>`__.

Language-specific APIs
~~~~~~~~~~~~~~~~~~~~~~

-  `JavaScript library <#>`__
-  `.NET library <#>`__

Didn't find a library for your language? You can always use `HTTP
API <#>`__ directly.

People, Events and Properties
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Tracktor.io processes data that is received from **people**, **events**
and **properties**. In brief, we obtain information about who your users
are and what they do on your site. It helps you to divide your users
into segments. **Person** is the primary unit for all calculations and
reports. Each person is automatically assigned a unique ID. Persons are
divided into two main groups: Identified and Anonymous. They can be
identified by email, Facebook ID, Twitter ID. If a person is not
identified, he is considered anonymous. The examples of anonymous IDs
are:

-  a-3bxuAxl18r02UsdTUdk1Ri9;
-  a-xrWNmprAoEJM507bt7m5LKg.

After Tracktor.io is integrated with your application or site, you can
track a person's actions from the moment of the first visit and continue
tracking him after his registration.

**Events** are actions that visitors do with your application. Here are
some examples of possible events: Site visit, Home page visit, Landing
page visit, Application download, Sign up, Opened Email, Subscription,
Purchase. These are just a few examples of possible events.

**Properties** show additional information about users of your site.
Properties give you valuable details about users, which allows you to
divide them by segments. Let's take a look at a few examples of dividing
users into segments by properties:

-  Gender. You will be able to separate male and female users into
   different segments.
-  City. Specifying "New York" or "London" will show you all users from
   corresponding cities.
-  Age. By specifying "Age greater than or equal to 25", the application
   will create a segment with people who are at least 25 years old.
-  Plan. If you specify Basic subscription plan, you will see all users
   which currently use your Basic subscription profile.
-  Also, you can give properties any name you like.

What should I track?
~~~~~~~~~~~~~~~~~~~~

You can choose which events you want to track with Tracktor.io. First,
define your business goals. For example, you are the owner of a video
service. Before a customer purchases a subscription or movie, he would
perform one of the following actions: search, movie watch, review. With
the knowledge of what a user does with your product, you can suggest
your premium services to him, like subscription, premieres of the best
movies, or valuable offers from your partners

Real-time Segmentation
----------------------

Imagine you have 100000+ users. You need to send a newsletter only to
users who have purchased a specific item in the last 2 months. Segments
can help you with this. Tracktor.io gives you a wide range of conditions
for grouping users into segments. A segment is a group of people which
is defined by the rules or conditions that you set. Users are
automatically added to a segment after you specify the segmentation
rules. Segments are updated in real-time. If some users match the
conditions specified by a segment, they are automatically added to that
segment. Also, users are automatically deleted from the segment after
removal from the People list.

Tracktor.io segments people by **Properties**, **Events** and
**Companies**.

Tracktor.io can segment your users by **Properties**. The examples of
properties are: City, Company, ZIP code, Country, Age. Properties can be
managed at Account Settings -> User properties.

You can create a segment based on occurred **Events**. For example, you
want to select all people who bought a specific product. First, you must
create the Event, which will track the purchase of this product (i.e.
Article2). Then, you can create a segment, which will be based on
Article2 event.

With Tracktor.io you can group people by **Companies**. With this option
you will see all people that belong to a specific company.

Dashboard, Metrics & Funnels
----------------------------

With the dashboard, which is called Analytics, you can look at the
statistics of your site. Statistics are mainly represented by Metrics
and Funnels, which are updated at least once every 2 hours. Also, you
can update a specific Metric or Funnel individually by editing it.

How Funnels work?
~~~~~~~~~~~~~~~~~

With Funnels your customers will be able to see the conversion rate
between events. Lets take as an example the funnel which has steps
A,B,C,D. These steps can be done by a customer in a different order.

1. The steps A->B->C->D are done in this exact order. Tracktor.io will
   calculate this case as full conversion.
2. The steps are A->B->C->E->D. Tracktor.io will consider this sequence
   of steps as a conversion. Also, this case shows the example of a
   loose ordering.
3. The order of steps is: A->B->D. In this case Tracktor.io will not
   count this sequence as a conversion. The customer will not be shown
   in the funnel after step B.

The funnels are calculated based on unique users. Thus, you will see the
exact number of customers who completed the steps in a funnel sequence.

How Metrics work?
~~~~~~~~~~~~~~~~~

Metrics show the performance of individual events and properties. Each
metric shows you data both for predefined and for custom date ranges.
Besides, from each metric you will receive information about daily
maximum, minimum and average values.

Types of Metrics
~~~~~~~~~~~~~~~~

With Tracktor.io you can create following types of metrics:

-  **Number of People Who Did Event**

Here each person is counted maximum 1 time, even if he completed this
event multiple number of times.

-  **Number of Times Event Happened**

This metric counts each time the event was completed, regardless of the
number of people involved. For example, this metric is suitable for
calculation of unique Site visit.

-  **Average Number of Times Event Happened Per Person**

It calculates the number of times an event was done, divided by the
number of people who completed this event.

-  **Conversion Rate**

**Conversion Rate** metric calculates conversion rate between two
specified events. You can make it a repeatable event (product purchases
for example) or a non-repeatable event (like an account signup).

-  **Average Time Between Events**

It calculates time elapsed between completion of first event and second
event, divided by number of people who completed both events.

-  **Total Value for Property**

Total Value for Property metric Calculates total value of a numeric
property across all people who meet a specific property criteria.

-  **Average Value for Property**

The metric calculates total value of a numeric property, divided by
either all people, by people who did a specific event, or by the number
of times the property was set.

Tutorials
---------

1.Tracking your first Event?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Any activity performed by your users can be sent to Tracktor.io. Events
are used for sending this activity. To start tracking events first
carefully read the information in section `Quick Start Guide <#>`__. We
have two main groups of events: Auto events and Advanced events.

To configure **Auto events** in our system:

-  Login to http://app.tracktor.io
-  Go to Application settings
-  Select "Auto Events" in the left panel
-  Click "Add new event"
-  In "Add event" dialog in put event name. Select "Page views" or
   "Element click" in "Generated by" field. For **Page views** you will
   see a quick guide with syntax and expression examples. Also, you can
   use our expressions testing tool. To configure an event on **Element
   click** you will have to input CSS selector. The list of available
   CSS syntax with examples of use can be found under the CSS selector
   field.
-  After all fields are filled in click "Save" button

Besides User events our system already has built-in events - **Advanced
events**. For using Advanced events:

-  Go to Application settings
-  Select "Advanced Events" in the left panel
-  You will see a list of available advanced events. You can only
   enable/disable them. Currently we have three advanced events: Visited
   Site, Canceled Cart and Abandoned Cart. For *Canceled Cart* and
   *Abandoned Cart* events you can set the time of user inactivity after
   which the event will be generated
-  Click "Save" button to save changes.

2.Creating your first Segment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you don't need to send an email or in-app message to ALL your users,
you can divide them into segments. With Tracktor.io tool you can create
segments of users, based on 4 types of data: **Property**, **Event**,
**Company** and **Asset Activity**.

For creating segment do the following:

-  Go to People->Segments. Click "New segment"
-  In the next window input desirable segment name
-  Then define the conditions. As was told above, there are four
   conditions which you can use for segmenting people: Property, Event,
   Company and Asset Activity. Let's say you want to create a segment of
   people who visited a specific page at your application. For this page
   visit you have an Event, which is called "Item1" To create a segment
   based on this Event select "Event" in "Segment by" field. In "Users
   who" drop-down boxes select "have performed the event" and select
   event "Item1"
-  When creating segments you can also use arguments and period of event
   occurrence. Also, you can add other conditions with "OR" and "AND"
   options
-  Click "Create segment" button. You will see a window with list of
   segments. If your segment has few complex conditions, you will
   probably have to wait until it will be calculated.

3.Creating your first Funnel
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Funnels** are used for tracking steps which people do to become your
customers. Once you understand at which steps people stop from being
your customers, you will know which parts of your business requires the
most attention. With this knowledge your business will engage more
customers. Let's create your first Funnel with Tracktor.io. As an
example let's take the case with your own video service provider. Here
are the steps users would do to become you customers:

-  **Site Visit**. This is the first step of your funnel.
-  **Free movie view**. Let's imagine your service a list of free movies
   to watch. After watching them users would know that you give access
   to movies with great HD or UltraHD quality.
-  **Registration** Lets imagine that in order to have even more movies
   in HD quality user has to make a registration.
-  **Upgrade to a Premium paid plan** This is a paid service, which
   brings your company a real profit. This service gives a user an
   access the top-rated movies in HD and UltraHD quality. Also, this
   service gives access to Premiers and movies in 3D.

Having this funnel will give the information about steps, at which users
stop before they actually start paying for your service. Now, lets
create a Funnel. ####How to build your Funnel

-  First, login to http://app.tracktor.io
-  Then click Analytics tab at the top of the main window
-  Click "Create new"->"Funnel"
-  Input funnel name. Click small "+" button above "Save funnel" button
-  In drop-down list of "Users who have performed the event" select
   **"Site visit"** event
-  Click small "+" button above "Save funnel" button
-  In drop-down list of "Users who have performed the event" select
   **"Free movies"** event
-  Click small "+" button above "Save funnel" button
-  In drop-down list of "Users who have performed the event" select
   **"Registration"** event
-  Click small "+" button above "Save funnel" button
-  In drop-down list of "Users who have performed the event" select
   **"Upgrade to Premium"** event
-  Click "Save funnel" button

After these steps are done, you will see a diagram, showing you how many
people finished each step. Click on event names with hyperlinks under
diagram will show you a list of people that have passed each stage of a
funnel. The table will show you ID, email and last action of each user.
You can select a suitable date range, either predefined or custom.

Remember, that events will most probably be different. These names are
suitable just for the example above. Also at any time you can add or
remove steps from the funnel, or delete it.

4.Creating your first Metric
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Tracktor.io supports creation of Metrics. With metrics you will be able
to observe performance of the individual parameters of your site. The
following is the example of Metric creation, where you want to see site
visits during last month:

-  Go to Analytics
-  Click "Create new"->"Funnel"
-  Select "Number of People Who Did Event" metric. Click "Next" button
-  In "Specify metric parameters" window input metric name "Site
   visits". In "Event" drop-down list select **Site Visit** event
-  Click "Add metric" button.

After these steps you will see the Analytics Dashboard. It contains all
Metrics and Funnels in one place. Here, you will see your "Site visits"
metric as a block with small histogram. By default, it will show you the
number of site visits for this week. To view the "Site visits" metric in
detail do the following:

-  Click on metric name to open it
-  You will see a diagram where the default time range is "Last 7 days"
-  In Date range drop down list select the desired time period
-  To the left of the diagram additional statistics will be shown: Daily
   maximum, Daily minimum, Daily average and Number of people who did
   this event.

5.Creating your first Workflow Campaign
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

With the **Workflow Campaigns** feature you can create truly effective,
flexible campaigns. Campaigns have notable advantages over ordinary
newsletters. First, they work in a real time. Second, they have rich
toolbox which includes triggers, filters and actions. And last but not
least, you can observe the amount of executed actions with a schematic
diagram (or as we call it - working area). Again, lets take as an
example a video service provider. You need to send a special proposal
letter to all users who are still on a free account and have watched
movies at least 20 times. For creating Workflow campaign do the next
steps:

-  Go to Campaigns-> Workflow
-  Click the "New Campaign" button at the upper right corner of the
   window
-  In the next window input the campaign name and click "Add" button
-  A window for editing workflow campaigns will be opened. It is divided
   into three parts: a toolbox, a working area and a block with letters
   list
-  Leave "For all people" block as it is
-  Select trigger "Person Event". In the events drop-down list select
   "Movie watch"
-  Next, select the "Entry limit" filter. Input 20 to "Entries count
   limit" field
-  Select the Person property filter with NOT free Plan. Connect this
   property to "No" of "Entry limit" filter
-  In Toolbox select "Sent Letter" action. Compose an email that you are
   going to send and attach it to "Sent letter" action
-  Connect "Send letter" action to the Yes option of the Person property
   filter
-  Click Save. Click "Run"

Now your campaign will instantly send your newsletter to each user who
watched at least 20 movies and has Free subscription plan. Once the
campaign is active, you can monitor its performance in a real time. All
information about your campaign is gathered in one place, which makes it
very easy to use for your whole team.

6.Creating your first Newsletter
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Newsletters play a powerful role for communication with your customers.
For creating a **Newsletter**:

-  Go to Campaigns->Manual
-  Click "New Campaign" button
-  Input campaign name and specify whether it will be sent to all users,
   or to specific segments. Click "Save settings"
-  Click "Edit content"
-  In the "Asset settings" dialog window input letter subject and sender
-  Tracktor.io gives you the opportunity to create a newsletter in three
   different modes: Drag&Drop, Html WYSIWYG and Plain text
-  For our example we select the Drag&Drop letter type. You will see the
   Drag&Drop editor, where you must click the "Choose layout" button.
   Leave "One column" as default and click "Choose"
-  Then you can edit the template by adding there required text,
   pictures and links. To edit different sections and blocks of your
   newsletter - select them and modify using the editor in the right
   part of the window
-  By clicking the "Preview" button you can see the final appearance of
   the letter to be sent
-  When editing is done click "Save and Close". "Edit Newsletter
   Campaign" window will be opened Click "Send Campaign" for sending
   your newsletter to specified recipients.

After the letter is sent, you will see how many letters were sent,
opened, or bounced, and how many times links were clicked. Also, you
will be able to observe the results of your campaign in detail with the
"Letter statistics detail" chart and in the "Letter activity" table. The
Letter activity table will show you detailed results for every
recipient, which will include: Status, Person ID, Email, Subject, Opens
and Clicks. By clicking on subject of the letter you will see a letter
preview in a separate window.

7.Adding another application
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You can integrate Tracktor.io with few different applications. For
adding a new application do the following:

-  On a main screen point a mouse on a current site name. Click "Manage"
-  Above sites list click "Add new site"
-  Input your integration settings in the next window. Click "Re-check"
   button to verify integration settings
-  Next, input communication widget settings and Save changes.

To remove a site or application go to Account Settings->Sites. Click the
"Remove" button to remove the site.

8.Sending a manual in-app message
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

With our built-in in-app messenger you will be able to manually send
messages to chosen users. To do this you have to:

-  Go to people list
-  Select either all people on the list or select them one by one. You
   will see a green button above the people list table. Click it
-  Select the "Send Message" option
-  Input message text
-  You will have two options for sending this message: in-app message
   and Email. In-app is set by default
-  Click "Send". The message will be sent to all selected users. They
   will see it after they log in to your application. With our in-app
   messenger they will be able to answer you. All messages will be
   automatically transferred to conversations section of our site.
