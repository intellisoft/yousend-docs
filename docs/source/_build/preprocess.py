import os
import glob
import subprocess
import io

gitPath     = 'C:\Program Files\Git\\bin\git.exe'
docsPath    = os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
sourcePath  = docsPath + '\markdown'
targetPath  = docsPath + '\source'
buildPath   = targetPath + '\_build'
targetFiles = glob.glob(targetPath + '\*.rst')
sourceFiles = glob.glob(sourcePath + '\*.md')

print('Removing current reStructured text files')

for targetFile in targetFiles:
    subprocess.call([gitPath, 'rm', '-f', targetFile]);


print('\nConverting markdown files to reStructured text')

for sourceFile in sourceFiles:
    targetFile = targetPath + '\\' + os.path.basename(sourceFile).replace(".md", ".rst")
    subprocess.call(['pandoc', '--from=markdown', '--to=rst', '--output=' + targetFile, sourceFile])
    subprocess.call([gitPath, 'add', targetFile])
    print('converted ' + targetFile)

print('\nBuilding index')
targetFile   = targetPath + '\index.rst'
targetStream = io.open(targetFile, 'w+t');
targetStream.write(open(buildPath + '\index.rst.tpl').read())

for sourceFile in sourceFiles:
    toc = os.path.basename(sourceFile).replace(".md", "")
    targetStream.write(u'    ' + toc + '\n')

targetStream.write(u'\n')
targetStream.close()
subprocess.call([gitPath, 'add', targetFile])

print('\nDone')
