JavaScript library reference
============================

Installation
------------

To install Javascript library, you should get a snippet that can be
found at *Application settings->Integration settings* and drop it into
the ``head`` of your site. It looks like this:

::

    <script type="text/javascript">
      (function() {
        window.tio = window.tio || [];
        var d = document, f = d.getElementsByTagName('script')[0],
        t = 'YOUR_TOKEN_HERE',
        s = d.createElement('script'), m = ['track', 'identify','setProperties'],
        r = function (e) {return function () { tio.push([e].concat(Array.prototype.slice.call(arguments, 0))) }};
        s.type = 'text/javascript'; s.async = true; s.src = 'https://tracktor.s3.amazonaws.com/scripts/'+t+'.js';
        f.parentNode.insertBefore(s, f); for(var i in m) tio[m[i]] = r(m[i]);YS=tio; })();
    </script>

After that, you can find Tracktor.io library methods within ``tio``
namespace. Library uses HTML5 Local storage to keep track of current
user identity and falls back to cookies if it's not available.

People api
----------

tio.identify(personId)
~~~~~~~~~~~~~~~~~~~~~~

When a person opens a page with tracking script the library
automatically generates unique anonymous id, like
``a-3bxuAxl18r02UsdTUdk1Ri9``. All events you track will be assigned to
this id. But when person signs in, you'll want to identify him with
different id you provide. This can be any unique string that identifies
this person: user id in database, login, email (only if it cannot be
changed).

Sample call:

::

    tio.identify('unicorn');

If the current person identity is anonymous, all it's data is merged
into the provided profile. This way you can start tracking user activity
from the point when you don't know who it is and merge all data when
this user signs in or registers.

If the current person identity is non-anonymous, this method just
switches current person without merging any data.

tio.setProperties(properties)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You can set any additional info about current person by calling this
method. A person property can have any data type you want. It can even
be an array or object. However, there are some predefined properties,
that have fixed data type. You can read about them
`here <./002%20API.html#setting-person-properties>`__.

Sample call:

::

    tio.setProperties({
      email: 'unicorn@example.com',
      first_name: 'Dave',
      last_name: 'Johnson',
      age: 25,
      billing_plan: 'free'});

**Note**: if a person already has some property and it's not specified
in properties object, it won't be changed. If you want to remove a
particular property, you should assign ``null`` value to it:
``{delete_me: null}``

tio.track(eventName, args)
~~~~~~~~~~~~~~~~~~~~~~~~~~

To track an event for the current person, you should call ``track``
method. Just like with ``setProperties`` method, ``args`` parameter must
be an object with any properties you need.

Sample call:

::

    tio.track('rated-product', {
      productid: 33156,
      category: 'Consumer electronics',
      rating: 5,
      comment: 'Very good product!'});

Secure mode
-----------

Due to the way we receive person data and events, it is possible to fake
person identity by third person (attacker). Attacker can send any event
"from" any other person, which may force person to enter invalid segment
or trigger workflow campaign that should not be run for that person. But
the most dangerous thing is that attacker can access conversation log,
which may contain sensitive data.

In order to avoid this issue, we implemented **Allow only signed data**
option. |Signed mode option|

This option ensures that Tracktor will receive only data that has valid
signature. To do that, you must generate signature of person identity
with HMAC-SHA256 algorithm, using signature secret provided. This
signature must be generated on server side of your application, so
attacker won't be able to access the secret code.

Example usage: For example, your secret code is
``2D8CarQ4rX3WvH6TK7DpJDk5``, person identity - ``5115``

1. Generate signature of person identity on backend:

::

    // C#
    private string GenerateSignature(string key, string identity)
    {
        var keyBytes = Encoding.UTF8.GetBytes(key);
        using (var hmac = new System.Security.Cryptography.HMACSHA256(keyBytes))
        {
            var signature = hmac.ComputeHash(Encoding.UTF8.GetBytes(identity));
            return BitConverter.ToString(signature).Replace("-", string.Empty);
        }
    }

    var signature = GenerateSignature("2D8CarQ4rX3WvH6TK7DpJDk5", "5115");
    // 9622E88BED0B5BC186286CB9504A02E5F3B4207DCA0AB23CA4FBDDFC15627E2F

2. Send both identity and signature on frontend, as you usually do.
3. Make identify call with signature:

   ::

       tio.identify('5115', '9622E88BED0B5BC186286CB9504A02E5F3B4207DCA0AB23CA4FBDDFC15627E2F');

4. Enable **Allow only signed data** option
5. Now you are protected from identity theft

In-app messenger widget
-----------------------

Javascript library is bundled with a messenger widget that provides
in-app style messaging with users. Widget settings can be configured via
Tracktor's UI, but you can override them dynamically, if you need to.

All available settings are provided in following example:

.. code:: javascript

    tio.setMessengerConfig({
        icon: 'https://cdnjs.cloudflare.com/ajax/libs/fatcow-icons/20130425/FatCow_Icons32x32/world.png', // override closed widget icon
        welcomeText: '', // override text, that person sees when opens widget, that has no conversations yet
        headerIcon: '', // override icon to the left of header text
        headerText: 'Tracktor.io Messenger',
        positionX: 'right', // right/left
        positionY: 'bottom', // top/bottom
        iconMargin: {
            "X": "10px",
            "Y": "10px"
        },
        display: true // display: false would hide widget
    });

You can specify only options that you want to override:

::

    if (/* Widget should be hidden */) {
      tio.setMessengerConfig({ display: false }); // Hide widget
    }

Binding to messenger events
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Messenger widget provides some events you can bind to:

*tio-messenger-opened* - occurs when person opens the widget

*tio-messenger-closed* - occurs when person closes the widget

*tio-message-sent* - occurs when person sends new message

*tio-message-received* - occurs when new user message received, during
widget closed state

Binding to events with JQuery:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

    $(document).on('tio-message-sent', function (e) {
      console.log('message sent: ' + e.originalEvent.data.msg);
    });

Binding to events with pure JS (cross-browser)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

    function addEvent(el, eventType, handler) {
      if (el.addEventListener) { // DOM Level 2 browsers
        el.addEventListener(eventType, handler, false);
      } else if (el.attachEvent) { // IE <= 8
        el.attachEvent('on' + eventType, handler);
      } else { // ancient browsers
        el['on' + eventType] = handler;
      }
    }

    addEvent(document, 'tio-message-sent', function (e) {
      console.log('message sent: ' + e.data.msg);
    });

E-commerce extension
--------------------

We have a specialized extension for e-commerce applications. With our
e-commerce api, you can track purchased, canceled and abandoned carts.

Cart object
~~~~~~~~~~~

Most of the following methods operate with cart object that must have
following structure:

::

    {
        totalprice: decimal, // total cart price, including any additional fees
        products:
        [
            {
                name: string, // name of the product
                price: decimal, // price of single product
                count: integer, // (optional) product quantity (1 by default)
                thumburl: string, // (optional) url to product's thumbnail image
                custom_property: any_type // You can attach any additional properties, if you need
            }
        ],
        custom_property: any_type // You can attach any additional properties, if you need
    }

Generally, a cart can have any additional properties you need. Here's a
sample cart object:

.. code:: javascript

    {
        totalprice: 3.98,
        products:
        [
            {
                name: "Marshmallows 1kg pack",
                price: 1.99,            
                color: "green"
            },
            {
                name: "Zephir 1kg pack",
                price: 1.99
            }
        ],
        paid_with: "Credit card",
        card_type: "VISA",
        shipping_method: "in-store pick up"
    }

tio.setCart(cart)
~~~~~~~~~~~~~~~~~

After you set a cart, abandoned cart tracking mode is activated. When
api doesn't receive any cart data for a configurable amount of time, the
cart is assumed abandoned and ``Cart abandoned`` event is generated
inside Tracktor.io. You can set a workflow on this event, to return the
person to your store and encourage them to make the purchase.

tio.cancelCart(cart)
~~~~~~~~~~~~~~~~~~~~

If a user cleans up your cart, you should call this method to stop
abandoned cart tracking.

tio.purchaseCart(cart)
~~~~~~~~~~~~~~~~~~~~~~

Marks cart purchased and generates ``Cart purchased`` event

.. |Signed mode option| image:: https://monosnap.com/file/pvgmLxXTSXF9UfxTPQg5xZMLGDqt31.png

