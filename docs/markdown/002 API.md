# HTTP API Reference
For maximum compatibility, all our HTTP api methods use HTTP `GET`. Please, ensure that arguments are url-encoded, when sending data.

All api methods have 2 common parameters:

+ `sid` - your application token
+ `ts` - current Unix-style timestamp

## Setting person properties
You may attach any information to person profile, such as postal address, age, subscription status, etc. These properties can be used inside letters, segments and workflows for sending personalized messages, searching, grouping people, making decisions in workflow and many other helpful stuff.

Person property can have any data type you want. It can even be a JSON array or object. However, there are some predefined properties that have a fixed data type. You can read about them [here](#).

Method URL:
```
https://api.tracktor.io/set
```

Parameters:

Name    Description
------  ---------
`sid`   Your application token
`ts`    Current Unix-style timestamp
`id`    Unique identity of person
`sign`  _(optional)_ Identity signature (See secure mode)  
`data`  JSON object, containing new property values

Note: if a person already has some property and it's not specified in `data` object, it won't be changed. If you want to remove a particular property, you should assign `null` value to it: `{delete_me: null}`

Example call:
```
http://api.tracktor.io/track?sid=00000000000&ts=1429186474&id=unicorn&data=%7Bbilling_plan%3A%27Gold%27%7D
```

### Predefined properties
There are some predefined properties, which provide additional features:

--------------------------------------------------------------------------------
Name                Description
--------------      ------------------------------------------------------------
`email`             stores person's email. You cannot send email messages to a
                    person if this property is not set.

`first_name`        stores person's first and last name. These properties can be  
`last_name`         updated automatically with social profile data, and they are
                    displayed in Tracktor's UI

`profile_image`     can specify a URL for profile image, which will be displayed in the
                    conversation widget and inside Tracktor's UI.
--------------------------------------------------------------------------------

## Tracking an event
Any person action can be tracked. Sometimes it's enough to record just the fact that something has happened, but it might also be helpful to include some additional data. It can be used in workflows and letters, just like person properties.

Method URL:
```
https://api.tracktor.io/track
```

Parameters:

Name     Description
-----    --------
`sid`    Your application token
`ts`     Current Unix-style timestamp
`id`     Unique identity of person
`sign`   _(optional)_ Identity signature (See secure mode)  
`action` Name of tracked event
`data`   _(optional)_ JSON object, containing all associated data of tracked event

Example call:

```
http://api.tracktor.io/track?sid=00000000000&ts=1429186474&id=unicorn&action=rated%20item&data=%7Bcategory%3A%27tires%27%2Citem%3A%27Acme%20inc%20R14%27%2Crating%3A5%7D
```

## Merging people profiles

Sometimes you'll want to merge two person profiles, when you know that it's the same person. It can be archived using the alias method. After person profiles are aliased, all existing data is merged to a single profile and any new data goes to the same person, even if recorded using different ids.

**Warning** There is no way to revert this operation, so be careful.

Method URL:
```
https://api.tracktor.io/alias
```

Parameters:

Name        Description
-----       --------
`sid`       Your application token
`ts`        Current Unix-style timestamp
`srcid`     Unique identity of source person profile
`destid`    Unique identity of destination person profile
`destsign`  _(optional)_ Destination identity signature (See secure mode)  


Example call:
```
https://api.tracktor.io/track?sid=00000000000&ts=1429186474&srcid=a-x123124123&destid=unicorn
```
